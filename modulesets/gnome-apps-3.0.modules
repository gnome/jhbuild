<?xml version="1.0"?><!--*- mode: nxml; indent-tabs-mode: nil -*-->
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<!-- vim:set ts=2 expandtab: -->
<moduleset>
  <repository type="git" name="git.gnome.org" default="yes"
      href="git://git.gnome.org/"/>
  <repository type="svn" name="inkscape.sf.net"
      href="https://inkscape.svn.sourceforge.net/svnroot/inkscape/"/>
  <repository type="svn" name="svn.abisource.com"
      href="http://svn.abisource.com/"/>
  <repository type="git" name="git.clutter-project.org"
      href="git://git.clutter-project.org/"/>
  <repository type="git" name="git.freedesktop.org"
      href="git://anongit.freedesktop.org/git/"/>
  <repository type="bzr" name="launchpad.net"
      href="http://code.launchpad.net/"/>
  <repository type="git" name="git.kernel.org"
      href="git://git.kernel.org/pub/scm/"/>
  <repository type="git" name="git.fedoraproject.org"
    href="git://git.fedoraproject.org/git/hosted/"/>
  <repository type="tarball" name="gnome.org"
    href="http://download.gnome.org/sources/"/>
  <repository type="tarball" name="cairo.org"
    href="http://cairographics.org/"/>
  <repository type="git" name="github.com"
    href="git://github.com/"/>
  <repository type="tarball" name="mono.net"
    href="http://go-mono.com/sources/"/>

  <include href="gnome-suites-core-3.0.modules"/>

  <!-- NOTE: gnome-suites-core-3.0 and gnome-suites-core-deps-3.0
       are supposed to remain self-contained. If apps have dependencies
       that are not covered there, add them in here
    -->

  <autotools id="gtkhtml" autogenargs="--enable-gtk3">
    <branch revision="gtk3"/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gtk+-3"/>
      <dep package="enchant"/>
      <dep package="iso-codes"/>
      <dep package="libsoup"/>
      <dep package="gnome-icon-theme"/>
    </dependencies>
  </autotools>

  <autotools id="gtk-sharp" autogen-sh="bootstrap-2.12">
    <branch repo="github.com" module="mono/gtk-sharp.git" checkoutdir="gtk-sharp"
            revision="gtk-sharp-2-12-branch"/>
    <dependencies>
      <dep package="mono"/>
      <dep package="gtk+"/>
    </dependencies>
    <suggests>
      <dep package="libglade"/>
    </suggests>
  </autotools>

  <autotools id="gtksourceview-3" autogenargs="--enable-compile-warnings=maximum">
    <branch checkoutdir="gtksourceview-3" module="gtksourceview"/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="libxml2"/>
      <dep package="shared-mime-info"/>
      <dep package="gtk+-3"/>
    </dependencies>
  </autotools>

  <tarball id="gtk-vnc" version="0.4.2" autogenargs="--with-gtk=3.0">
    <source href="http://download.gnome.org/sources/gtk-vnc/0.4/gtk-vnc-0.4.2.tar.bz2"
            hash="sha256:96e948b62984a1a3a32fd83793fb645c493c1bc93b303eb831ecafee01a956d2"
            md5sum="68fdeb71844c49d5d5ab4f32cbc8bddb" size="438062"/>
    <dependencies>
      <dep package="gtk+-3"/>
    </dependencies>
    <patches>
      <!-- fix from git, drop with 0.4.3 -->
      <patch file="gtk-vnc.drawable.patch" strip="1"/>
    </patches>
  </tarball>

  <tarball id="libgda" version="4.2.0" autogenargs="--with-java=no --disable-introspection">
    <source href="http://download.gnome.org/sources/libgda/4.2/libgda-4.2.0.tar.bz2"
            hash="sha256:2e639957f50719957cea5326b0d95511794322a54f9ef8547d3241f409765441"
            md5sum="4b00c2b61430b2a0ea00e0332d1e8ef4" size="13932542"/>
    <dependencies>
      <dep package="glib"/>
      <dep package="libxml2"/>
    </dependencies>
  </tarball>

  <autotools id="libgdiplus">
    <branch module="libgdiplus/libgdiplus-1.2.6.tar.bz2" version="1.2.6"
            repo="mono.net" 
            hash="sha256:5eb1a7a8d3b75c751eac6075239469059238082aee1963bc6a172b3073046cfe"
            md5sum="dfe8e43a49b4aa40ab5b7cf53bf83675" size="2897855">
    </branch>
    <dependencies>
      <dep package="glib"/>
      <dep package="cairo"/>
      <dep package="fontconfig"/>
    </dependencies>
  </autotools>

  <autotools id="mono">
    <branch module="mono/mono-2.4.2.2.tar.bz2" version="2.4.2.2"
            repo="mono.net" 
            hash="sha256:e3b965c56a669c3012856dcc17b7400d28505dd4bb8a3c6eb3e653bb41b5e6ba"
            md5sum="54aac9b914c5a4dc81c2bfd058df1c93" size="24813167"/>
    <dependencies>
      <dep package="libgdiplus"/>
      <dep package="sqlite3"/>
    </dependencies>
  </autotools>

  <autotools id="mono-addins">
    <branch module="mono-addins/mono-addins-0.4.tar.bz2" version="0.4"
            repo="mono.net"
            hash="sha256:d0fa0eec4e95c8cdfefc8961d7666d6137064df1b599a6489acf17e22fbc3e2e"
            md5sum="3b7f3f6e55c95413df184d0e4c9233e4" size="304402"/>
    <dependencies>
      <dep package="mono"/>
      <dep package="gtk-sharp"/>
    </dependencies>
  </autotools>

  <tarball id="ndesk-dbus" version="0.6.0">
    <source href="http://www.ndesk.org/archive/dbus-sharp/ndesk-dbus-0.6.0.tar.gz"
            hash="sha256:252322f18e906ba7e64cecd0baec08d1a0109206777a5507a713e12def126424"
            md5sum="5157ba105c9ac491f6e900bc78d1791f" size="122303"/>
    <dependencies>
      <dep package="mono"/>
    </dependencies>
  </tarball>

  <tarball id="ndesk-dbus-glib" version="0.4.1">
    <source href="http://www.ndesk.org/archive/dbus-sharp/ndesk-dbus-glib-0.4.1.tar.gz"
            hash="sha256:0a6d5fe7be55b6301615d71b89507b712f287b4ba498b798301333ffabe06769"
            md5sum="7faf8770b214956fa9de009def550826" size="85471"/>
    <dependencies>
      <dep package="ndesk-dbus"/>
    </dependencies>
  </tarball>

  <autotools id="opal" autogenargs="--disable-vpb"
             check-target="false" supports-non-srcdir-builds="no">
    <branch module="opal/3.8/opal-3.8.3.tar.bz2" repo="gnome.org"
            version="3.8.3"
            hash="sha256:d775626d786028357a8535fb2ff8f04bce9c1bff51db0e962f0939c2b988767f"
            md5sum="ff7bf6530af7d5113ba0a7604540c887" size="7452144"/>
    <dependencies>
      <dep package="ptlib"/>
    </dependencies>
  </autotools>

  <autotools id="ptlib" check-target="false" supports-non-srcdir-builds="no">
    <branch module="ptlib/2.8/ptlib-2.8.3.tar.bz2" repo="gnome.org"
            autogen-sh="configure"
            version="2.8.3"
            hash="sha256:076afde4e53e5fd0989adc344c3741aea8342b105c3e879e2f4f9a42ef36793e"/>
  </autotools>

 <autotools id="pycairo">
    <branch module="releases/pycairo-1.8.2.tar.gz" version="1.8.2"
            repo="cairo.org" 
            hash="sha256:77a8cbe388fd66825056744f2fc5c58b3afc247748bc2c777751cc0c2eb30a2f"
            md5sum="bbfc0c2fef08dfb0c5b8baa5f0b67a87" size="519064">
    </branch>
    <dependencies>
      <dep package="cairo"/>
    </dependencies>
  </autotools>

  <autotools id="pygobject">
    <branch/>
    <dependencies>
      <dep package="glib"/>
      <dep package="gobject-introspection"/>
      <dep package="pycairo"/>
    </dependencies>
  </autotools>

  <autotools id="pygtk">
    <branch/>
    <dependencies>
      <dep package="pygobject"/>
      <dep package="gtk+"/>
      <dep package="pycairo"/>
    </dependencies>
  </autotools>

  <autotools id="gdl">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="librsvg"/>
      <dep package="gtk+-3"/>
    </dependencies>
  </autotools>

  <!-- Apps start here -->

  <autotools id="abiword">
    <branch repo="svn.abisource.com"/>
    <dependencies>
      <dep package="glib"/>
      <dep package="gtk+"/>
      <dep package="libxml2"/>
      <dep package="libgsf"/>
      <dep package="wv"/>
    </dependencies>
    <suggests>
      <dep package="enchant"/>
      <dep package="gnome-vfs"/>
      <dep package="gucharmap"/>
    </suggests>
  </autotools>

  <autotools id="banshee" autogenargs="--disable-daap --disable-boo --disable-ipod --disable-mtp">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gconf"/>
      <dep package="gstreamer"/>
      <dep package="gst-plugins-base"/>
      <dep package="gst-plugins-good"/>
      <dep package="mono"/>
      <dep package="gtk-sharp"/>
      <dep package="taglib-sharp"/>
      <dep package="mono-addins"/>
      <dep package="ndesk-dbus"/>
      <dep package="ndesk-dbus-glib"/>
      <dep package="sqlite3"/>
    </dependencies>
  </autotools>

  <autotools id="deja-dup">
    <branch repo="launchpad.net"
            module="~deja-dup-team/deja-dup/trunk"
            checkoutdir="deja-dup/"/>
    <dependencies>
      <dep package="dbus-glib"/>
      <dep package="gconf"/>
      <dep package="glib"/>
      <dep package="gnome-keyring"/>
      <dep package="gtk+"/>
      <dep package="libnotify"/>
      <dep package="libunique"/>
      <dep package="nautilus"/>
      <dep package="vala-trunk"/>
      <dep package="duplicity"/>
    </dependencies>
  </autotools>

  <autotools id="f-spot">
    <branch/>
    <dependencies>
      <dep package="gnome-sharp"/>
    </dependencies>
  </autotools>

  <autotools id="gthumb">
    <branch/>
    <dependencies>
      <dep package="gtk+"/>
    </dependencies>
  </autotools>

  <autotools id="gimp" autogenargs="--disable-print --without-gnomevfs">
    <branch/>
    <dependencies>
      <dep package="gtk+"/>
      <dep package="intltool"/>
      <dep package="gegl"/>
    </dependencies>
    <suggests>
      <dep package="iso-codes"/>
      <dep package="gtkhtml2"/>
      <dep package="librsvg"/>
      <dep package="poppler"/>
      <dep package="gnome-keyring"/>
      <dep package="dbus-glib"/>
      <dep package="pygtk"/>
    </suggests>
  </autotools>

  <autotools id="gnome-scan">
    <branch/>
    <dependencies>
      <dep package="gtk+"/>
      <dep package="gconf"/>
      <dep package="gegl"/>
      <dep package="gimp"/>
    </dependencies>
  </autotools>

  <autotools id="gnote">
    <branch/>
    <dependencies>
      <dep package="glibmm"/>
      <dep package="libxml2"/>
      <dep package="libxml++"/>
      <dep package="gtkmm"/>
      <dep package="gconf"/>
    </dependencies>
    <suggests>
      <dep package="libpanelappletmm"/>
    </suggests>
  </autotools>

  <autotools id="gnumeric">
    <branch/>
    <dependencies>
      <dep package="goffice"/>
      <dep package="libgsf"/>
      <dep package="pygtk"/>
    </dependencies>
  </autotools>

  <autotools id="inkscape">
    <branch repo="inkscape.sf.net" module="inkscape/trunk" checkoutdir="inkscape"/>
    <dependencies>
      <dep package="gtkmm"/>
      <dep package="libxslt"/>
    </dependencies>
  </autotools>

  <autotools id="monkey-bubble">
    <branch/>
    <dependencies>
      <dep package="gstreamer"/>
      <dep package="gst-plugins-base"/>
      <dep package="libxml2"/>
      <dep package="gconf"/>
      <dep package="librsvg"/>
      <dep package="libgnomeui"/>
    </dependencies>
  </autotools>

  <autotools id="rhythmbox">
    <branch/>
    <dependencies>
      <dep package="dbus-glib"/>
      <dep package="gst-plugins-base"/>
      <dep package="libgnome-media-profiles"/>
      <dep package="totem-pl-parser"/>
      <dep package="gnome-doc-utils"/>
      <dep package="gst-python"/>
      <dep package="pygtk"/>
      <dep package="libsoup"/>
      <dep package="libnotify"/>
    </dependencies>
    <suggests>
      <dep package="gnome-keyring"/>
      <dep package="avahi"/>
      <dep package="libmusicbrainz"/>
      <dep package="brasero"/>
      <dep package="libdmapsharing"/>
    </suggests>
  </autotools>


<!-- Featured Apps -->

  <autotools id="alacarte">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gnome-menus"/>
      <dep package="pygtk"/>
    </dependencies>
  </autotools>

  <autotools id="cheese">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="glib"/>
      <dep package="gtk+"/>
      <dep package="gconf"/>
      <dep package="gstreamer"/>
      <dep package="gst-plugins-base"/>
      <dep package="gst-plugins-good"/>
      <dep package="gudev"/>
      <dep package="clutter-gst" />
      <dep package="clutter-gtk"/>
      <dep package="libgee"/>
      <dep package="gnome-video-effects"/>
      <dep package="librsvg"/>
      <dep package="gnome-desktop"/>
      <dep package="evolution-data-server"/>
    </dependencies>
    <suggests>
      <!-- this is not an approved dependency, it is left as a suggest so a
           moduleset defining this module would get it working -->
      <dep package="mx"/>
    </suggests>
  </autotools>


  <autotools id="ekiga">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="glib"/>
      <dep package="gtk+"/>
      <dep package="libxml2"/>
      <dep package="libsigc++2"/>
      <dep package="opal" />
      <dep package="evolution-data-server"/>
    </dependencies>
    <suggests>
      <dep package="dbus"/>
      <dep package="dbus-glib"/>
      <dep package="avahi"/>
      <dep package="libsoup"/>
      <dep package="gstreamer"/>
      <dep package="gst-plugins-base"/>
    </suggests>
  </autotools>

  <autotools id="evolution" autogenargs="--disable-image-inline --disable-pst-import --disable-nm --enable-gtk3">
    <branch revision="gtk3"/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gtk+-3"/>
      <dep package="evolution-data-server"/>
      <dep package="gtkhtml"/>
      <dep package="gnome-desktop-3"/>
      <dep package="gnome-doc-utils"/>
      <dep package="libcanberra"/>
      <dep package="libical"/>
      <dep package="libunique-3"/>
    </dependencies>
    <suggests>
      <dep package="libnotify"/>
      <dep package="dbus-glib"/>
      <dep package="gstreamer"/>
    </suggests>
    <after>
      <dep package="mono"/>
    </after>
  </autotools>

  <autotools id="file-roller">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="rarian"/>
      <dep package="gnome-doc-utils"/>
      <dep package="nautilus"/>
    </dependencies>
  </autotools>

  <autotools id="gedit">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="libpeas"/>
      <dep package="gnome-doc-utils"/>
      <dep package="gtksourceview-3"/>
      <dep package="gsettings-desktop-schemas"/>
    </dependencies>
    <suggests>
      <dep package="iso-codes"/>
      <dep package="enchant"/>
    </suggests>
  </autotools>

  <autotools id="gnome-games">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="librsvg"/>
      <dep package="rarian"/>
      <dep package="gtk+"/>
      <dep package="pygtk"/>
      <dep package="clutter"/>
      <dep package="clutter-gtk"/>
    </dependencies>
  </autotools>

  <autotools id="gnome-nettool">
    <branch/>
    <dependencies>
      <dep package="gtk+-3"/>
      <dep package="gconf"/>
      <dep package="gnome-doc-utils"/>
    </dependencies>
  </autotools>

  <waf id="hamster-applet" check-target="false">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="pygtk"/>
      <dep package="pycairo"/>
    </dependencies>
    <suggests>
      <dep package="gnome-python-desktop"/>
    </suggests>
  </waf>

  <autotools id="seahorse">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gconf"/>
      <dep package="gtk+"/>
      <dep package="glib"/>
      <dep package="dbus-glib"/>
    </dependencies>
    <suggests>
      <dep package="libsoup"/>
      <dep package="libgnome-keyring"/>
      <dep package="gnome-keyring"/>
      <dep package="avahi"/>
      <dep package="dbus-glib"/>
      <dep package="libxml2"/>
      <dep package="libnotify"/>
    </suggests>
  </autotools>

  <autotools id="tomboy">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gtk-sharp"/>
      <dep package="ndesk-dbus"/>
      <dep package="ndesk-dbus-glib"/>
      <dep package="gnome-doc-utils"/>
      <dep package="mono-addins"/>
    </dependencies>
    <suggests>
      <dep package="gtkspell"/>
    </suggests>
  </autotools>

  <autotools id="vinagre" autogenargs="--disable-applet">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="avahi"/>
      <dep package="gtk+-3"/>
      <dep package="gconf"/>
      <dep package="gtk-vnc"/>
    </dependencies>
  </autotools>

  <autotools id="anjuta">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="glib"/>
      <dep package="gdk-pixbuf"/>
      <dep package="gtk+-3"/>
      <dep package="vte-3"/>
      <dep package="gdl"/>
      <dep package="libgda"/>
      <dep package="gtksourceview-3"/>
    </dependencies>
    <suggests>
      <dep package="glade3"/>
      <dep package="devhelp"/>
      <dep package="vala"/>
      <dep package="autogen"/>
    </suggests>
  </autotools>

  <autotools id="devhelp">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gtk+-3"/>
      <dep package="gconf"/>
      <dep package="WebKit"/>
      <dep package="libunique-3"/>
    </dependencies>
  </autotools>

  <autotools id="gdl">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="librsvg"/>
      <dep package="gtk+-3"/>
    </dependencies>
  </autotools>

  <autotools id="glade3">
    <branch/>
    <dependencies>
      <dep package="intltool"/>
      <dep package="gnome-common"/>
      <dep package="gnome-doc-utils"/>
      <dep package="gtk-doc" />
      <dep package="gtk+-3"/>
      <dep package="libxml2"/>
    </dependencies>
  </autotools>

  <autotools id="gnome-devel-docs">
    <branch revision="gnome-2-32"/>
    <dependencies>
      <dep package="gnome-doc-utils"/>
    </dependencies>
  </autotools>

<!-- Meta Modules -->

  <metamodule id="meta-gnome-apps-featured">
    <dependencies>
      <dep package="alacarte"/>
      <dep package="anjuta"/>
      <dep package="cheese"/>
      <dep package="devhelp"/>
      <dep package="ekiga"/>
      <dep package="evolution"/>
      <dep package="file-roller"/>
      <dep package="gedit"/>
      <dep package="glade3"/>
      <dep package="gnome-devel-docs"/>
      <dep package="gnome-games"/>
      <dep package="gnome-nettool"/>
      <dep package="hamster-applet"/>
      <dep package="seahorse"/>
      <dep package="tomboy"/>
      <dep package="vinagre"/>
    </dependencies>
  </metamodule>

</moduleset>
